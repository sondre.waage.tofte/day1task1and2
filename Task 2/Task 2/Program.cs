﻿using System;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int nameLength = 0;
            Console.Write("What is your name");
            string name = Console.ReadLine();
            nameLength = name.Length;
            string Startletter = name.Substring(0, 1);
            // Checking if the name start with a vowel to find out if it should be a or an
            bool IsVowel = false;
            string[] Vowels = new string[] { "a", "e", "i", "o", "u", "y" };
            for (int i = 0; i < 6; i++)
            {
                if (Startletter.ToLower() == Vowels[i])
                {
                    IsVowel = true;
                }
            }
            if (IsVowel == true)
            {
                Console.WriteLine("Hello {0}, your name is {1} characters long and starts with an {2}", name, nameLength, Startletter);
            }
            else
            {
                Console.WriteLine("Hello {0}, your name is {1} characters long and starts with a {2}", name, nameLength, Startletter);
            }
        }
    }
}
